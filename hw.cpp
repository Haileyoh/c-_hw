// # Homework
#include <iostream>
#include <string>
#include <cmath>
using namespace std;
// 1. Write a program that takes in a line as input (whole line) for your full
//    name.
//
//    ./hello
// 	What is your name?
// 	John Doe // input
// 	Prepare to die, John Doe!
//
// 	Hint: use getline

int main(){
  string name;
  cout << "What is your name?" << endl;
  getline(cin, name);
  cout << "Prepare to die, " << name << "!" << endl;
  return 0;
}

// 2. Prime checker: write a function that takes in integer as input and outputs a
//    boolean of whether it is prime or not
bool primeChecker(int x){
  if (x == 0 || x == 1)
    return false;
  if (x == 2)
    return true;
  if (x % 2 == 0)
    return false;
  for (int i = 3; i < int(pow(x, 0.5)); i = i+2){
    if (x % i == 0)
      return false;
    }
  return true;
}
