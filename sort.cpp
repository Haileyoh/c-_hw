#include <iostream>
#include <string>
#include <cmath>
#include <array>
using namespace std;

//selection sort

void selectionSort(int arr[]){
  for (int i=0; i < arr.length-1; i++){
    int minIndex = i;
    for (int j=i+1; j < arr.length; j++){
      if (arr[minIndex] > arr[j])
        minIndex = j;
    }
    int temp = arr[minIndex];
    arr[minIndex] = arr[j];
    arr[j] = temp;
  }
}

//insertion sort
void insertionSort(int arr[]){
  for (int k = 1; k < arr.length; k++){
    int temp = arr[k];
    int i = k-1;
    while (i >= 0 && arr[i] > temp){
      arr[i+1] = arr[i];
      i--;
    }
    arr[i+1] = temp;
  }
}

//bubble sort
void bubbleSort(int[] arr){
  int swaps = 0;
  while (swaps != 0){
    swaps = 0;
    for (int i = 1; i < arr.length; i++){
      if(arr[i] < arr[i-1]){
        swaps++;
        int temp = arr[i];
        arr[i] = arr[i];
        arr[i-1] = temp;
      }
    }
  }
}
